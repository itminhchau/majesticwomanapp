package com.minhchaudm.majesticwoman.data.api

import com.minhchaudm.majesticwoman.ui.model.*
import com.minhchaudm.majesticwoman.ui.model.Collection
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiService {
    @POST("user/login/")
    suspend fun signInUser(@Body signIn: SignIn): ObjectUser

    @POST("user/registration/")
    suspend fun registerUser(@Body register: Register): ObjectUser

    @GET("category/")
    suspend fun getCategories(@Header("Authorization") token: String): List<Category>

    @GET("collection/")
    suspend fun getCollections(@Header("Authorization") token: String): List<Collection>

    @GET("teacher/")
    suspend fun getInstructor(@Header("Authorization") token: String): List<Instructor>

    @GET("live_class/")
    suspend fun getClassLives(@Header("Authorization") token: String): List<LiveClass>

    @GET("course/")
    suspend fun getCourse(@Header("Authorization") token: String): List<Course>

    @GET("challenge/")
    suspend fun getChallenge(@Header("Authorization") token: String): List<Challenge>

    @GET("signature_course/")
    suspend fun getSignatureCourse(@Header("Authorization") token: String): List<SignatureCourse>
}
