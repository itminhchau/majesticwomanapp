package com.minhchaudm.majesticwoman.data.local

import android.app.Application
import android.content.Context

class DataTokenSharePreference(val context: Context) {
    companion object{
        fun newInstance(context: Context)= DataTokenSharePreference(context)
        private const val MY_SHARE_PREFERENCE = "MY_SHARE_PREFERENCE"
    }
    fun setString(key: String , value: String){
        val sharePreference = context.getSharedPreferences(MY_SHARE_PREFERENCE,Context.MODE_PRIVATE)
        val editor = sharePreference.edit()
        editor.apply {
            putString(key,value)
        }.apply()
    }
   fun getString(key: String):String{
        val sharePreference = context.getSharedPreferences(MY_SHARE_PREFERENCE,Application.MODE_PRIVATE)
        return sharePreference.getString(key,null)!!
    }
    fun removeString(key: String){
        val sharePreference = context.getSharedPreferences(MY_SHARE_PREFERENCE,Context.MODE_PRIVATE)
        val editor = sharePreference.edit()
        editor.remove(key)
        editor.apply()
    }

}