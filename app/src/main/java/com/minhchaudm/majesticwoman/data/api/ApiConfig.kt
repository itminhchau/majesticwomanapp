package com.minhchaudm.majesticwoman.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiConfig {
    private const val BASE_URL = "https://api.majesticwomangroup.com/api/v1/"
    //end cua no phai la /
    private val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
    private val retrofit = builder.build()
     val service: ApiService = retrofit.create(ApiService::class.java)
}