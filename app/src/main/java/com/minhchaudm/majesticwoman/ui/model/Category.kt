package com.minhchaudm.majesticwoman.ui.model

data class Category(
    val id: Int,
    val title: String,
    val image: ObjectImage,
    val courses_count: Int
)