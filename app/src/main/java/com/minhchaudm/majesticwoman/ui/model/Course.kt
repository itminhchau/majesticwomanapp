package com.minhchaudm.majesticwoman.ui.model

data class Course(
    val id: Int,
    val title: String,
    val description: String,
    val category: Category,
    val teacher: ObjectTeacher,
    val image: ObjectImage,
    val videos: List<Video>
)