package com.minhchaudm.majesticwoman.ui.model


data class SignatureCourse (
    val id: Int,
    val title: String,
    val description: String,
    val overview: String,
    val price: String,
    val product_id: String,
    val category: Category,
    val teacher: ObjectTeacher,
    val image: ObjectImage,
    val videos: Video,
    val created_at: String,
    val updated_at: String
        )