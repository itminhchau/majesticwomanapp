package com.minhchaudm.majesticwoman.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.minhchaudm.majesticwoman.R
import kotlinx.android.synthetic.main.fragment_dialog.view.*

class DialogSuggestionFragment : DialogFragment() {
    private lateinit var binding: ViewDataBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dialog, container, false)
        dialog?.apply {
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            setCanceledOnTouchOutside(false)
        }
        isCancelable = false
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.root.imageViewClose.setOnClickListener {
            dialog?.dismiss()
        }
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        fun newInstance() = DialogSuggestionFragment()
    }
}