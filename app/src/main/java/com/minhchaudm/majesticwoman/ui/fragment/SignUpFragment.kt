package com.minhchaudm.majesticwoman.ui.fragment


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.activity.MainActivity
import com.minhchaudm.majesticwoman.ui.model.Register
import com.minhchaudm.majesticwoman.utils.Status
import com.minhchaudm.majesticwoman.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.frament_signup.view.*


class SignUpFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var binding: ViewDataBinding
    private val viewModel: RegisterViewModel by lazy {
        ViewModelProvider(
            this,
            RegisterViewModel.RegisterViewModelFactory(requireActivity().application)
        )[RegisterViewModel::class.java]
    }
    private val callbackManager: CallbackManager by lazy {
        CallbackManager.Factory.create()
    }

    private val navController by lazy {
        findNavController()
    }

    override fun onStart() {
        super.onStart()
        auth = Firebase.auth

//        if (auth.currentUser != null){
//            (requireActivity() as MainActivity).showBottomNav()
//            navController.navigate(R.id.action_signUpFragment_to_homeFragment)
//        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.frament_signup, container, false)

        //  callbackManager = CallbackManager.Factory.create()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.root.button_sign_up.setOnClickListener {
//            navController.navigate(R.id.action_signUpFragment_to_signInFragment)
            signUpUser()
        }

        binding.root.text_login_here.setOnClickListener {
            navController.navigate(R.id.action_signUpFragment_to_signInFragment)
        }

        createRequest()
        binding.root.image_login_google.setOnClickListener {
            signInGoogle()
        }

        //click login facebook
        binding.root.image_login_facebook.setOnClickListener {
            signInFacebook()
        }
//        binding.root.login_button.setOnClickListener {
//            signInFacebook()
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
            }
        }
    }

    // SignUp user with api
    private fun signUpUser() {
        val editTextFistName = binding.root.editText_name.text.toString().trim()
        val editTextEmail = binding.root.editText_email.text.toString().trim()
        val editTextPassword = binding.root.editText_password.text.toString().trim()
        val editTextPasswordConfirm = binding.root.editText_confirm_password.text.toString().trim()
        if (TextUtils.isEmpty(editTextFistName) || TextUtils.isEmpty(editTextEmail) || TextUtils.isEmpty(
                editTextPassword
            ) || TextUtils.isEmpty(editTextPasswordConfirm)
        ) {
            Toast.makeText(requireContext(), "Please Enter Information", Toast.LENGTH_SHORT).show()
            return
        }
        val register = Register(editTextFistName, editTextEmail, editTextPassword)
        if (editTextPassword != editTextPasswordConfirm) {
            Toast.makeText(requireContext(), "Password not match", Toast.LENGTH_SHORT).show()
            return
        } else {
            viewModel.registerUser(register).observe(viewLifecycleOwner, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            Toast.makeText(requireContext(), "register success", Toast.LENGTH_SHORT)
                                .show()
                            navController.navigate(R.id.action_signUpFragment_to_signInFragment)
                        }
                        Status.ERROR -> {
                            Toast.makeText(
                                requireContext(),
                                "User with this email address already exists.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
        }

    }

    // SignIn Google with Firebase
    private fun createRequest() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
    }

    private fun signInGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    //val user = auth.currentUser
                    (requireActivity() as MainActivity).showBottomNav()
                    navController.navigate(R.id.action_signUpFragment_to_homeFragment)
                    //updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    //updateUI(null)
                }
            }
    }

    // SignIn Facebook with Firebase

    private fun signInFacebook() {

        LoginManager.getInstance()
            .logInWithReadPermissions(requireActivity(), listOf("email", "public_profile"))
        LoginManager.getInstance().registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {

                    Log.d(TAG, "facebook:onSuccess:$loginResult")
                    handleFacebookAccessToken(loginResult.accessToken)
                }

                override fun onCancel() {
                    Log.d(TAG, "facebook:onCancel")
                }

                override fun onError(error: FacebookException) {
                    Log.d(TAG, "facebook:onError", error)
                }
            })
//        binding.root.login_button.setPermissions("email","public_profile")
//        binding.root.login_button.fragment = this
//        binding.root.login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
//                override fun onSuccess(loginResult: LoginResult) {
//                    Log.d(TAG, "facebook:onSuccess:$loginResult")
//                    handleFacebookAccessToken(loginResult.accessToken)
//                }
//
//                override fun onCancel() {
//                    Log.d(TAG, "facebook:onCancel")
//                }
//
//                override fun onError(error: FacebookException) {
//                    Log.d(TAG, "facebook:onError", error)
//                }
//            })
//        // ...

    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    (requireActivity() as MainActivity).showBottomNav()
                    navController.navigate(R.id.action_signUpFragment_to_homeFragment)
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    // val user = auth.currentUser
                    Toast.makeText(
                        requireContext(), "Authentication success.",
                        Toast.LENGTH_SHORT
                    ).show()
                    // updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Toast.makeText(
                        requireContext(), "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    // updateUI(null)
                }
            }
    }

    companion object {
        const val TAG = "AAA"
        const val RC_SIGN_IN = 123
        fun newInstance() = SignUpFragment()
    }
}