package com.minhchaudm.majesticwoman.ui.model

data class Instructor(
    val id: Int,
    val first_name:String,
    val last_name: String,
    val description: String,
    val thumbnail: ObjectImage,
    val courses_count: Int
)