package com.minhchaudm.majesticwoman.ui.model

data class ObjectImage(
    val id: Int,
    val title: String,
    val file: String,
    val created_at: String
)