package com.minhchaudm.majesticwoman.ui.model

data class Challenge(
    val id: Int,
    val title: String,
    val description: String,
    val conditions: List<ObjectCondition>,
    val deadline: String,
    val image: ObjectImage,

    )