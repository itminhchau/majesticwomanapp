package com.minhchaudm.majesticwoman.ui.model

data class LiveClass(
    val id: Int,
    val title: String,
    val description:String,
    val thumbnail: ObjectImage,
    val teacher:ObjectTeacher,
    val live_url: String
)