package com.minhchaudm.majesticwoman.ui.model

data class ObjectCondition(
    val id: Int,
    val description: String
)