package com.minhchaudm.majesticwoman.ui.model

data class ObjectTeacher(
    val id: Int,
    val first_name: String,
    val last_name: String
)