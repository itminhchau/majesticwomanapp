package com.minhchaudm.majesticwoman.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.minhchaudm.majesticwoman.R

class MyPracticeFragment : Fragment() {
    private lateinit var binding: ViewDataBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_practice, container, false)
        return binding.root
    }



    companion object {
        fun newInstance() = MyPracticeFragment()
    }
}