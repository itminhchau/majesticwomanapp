package com.minhchaudm.majesticwoman.ui.model

data class ObjectFile(
    val id: Int,
    val title: String,
    val file: String,
    val created_at: String
)