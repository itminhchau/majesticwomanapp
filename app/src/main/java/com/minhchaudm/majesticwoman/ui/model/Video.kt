package com.minhchaudm.majesticwoman.ui.model

data class Video(
    val id: Int,
    val title: String,
    val description: String,
    val file720p: ObjectFile,
    val file1080p: ObjectFile,
    val thumbnail: ObjectImage,
    val course: Int

)