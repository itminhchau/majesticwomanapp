package com.minhchaudm.majesticwoman.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.adapter.*
import com.minhchaudm.majesticwoman.utils.Status
import com.minhchaudm.majesticwoman.viewmodel.HomeViewModel
import com.minhchaudm.majesticwoman.viewmodel.SharePreferenceViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {
    private val sharePreferenceViewModel: SharePreferenceViewModel by lazy {
        ViewModelProvider(
            this,
            SharePreferenceViewModel.SharePreferenceViewModelFactory(requireActivity().application)
        )[SharePreferenceViewModel::class.java]
    }
    private val homeViewModel: HomeViewModel by lazy {
        ViewModelProvider(
            this,
            HomeViewModel.HomeViewModelFactory(requireActivity().application)
        )[HomeViewModel::class.java]
    }
    private val mViewPagerAdapter: ViewPagerAdapter by lazy {
        ViewPagerAdapter(requireContext())
    }
    private val mCollectionAdapter: CollectionAdapter by lazy {
        CollectionAdapter(requireContext())
    }

    private val mInstructorAdapter: InstructorAdapter by lazy {
        InstructorAdapter(requireContext())
    }

    private val mLiveClassAdapter: LiveClassAdapter by lazy {
        LiveClassAdapter(requireContext(), onClickButtonLive)
    }

    private val mCourseAdapter: CourseAdapter by lazy {
        CourseAdapter(requireContext())
    }

    private val mChallengeAdapter: ChallengeAdapter by lazy {
        ChallengeAdapter(requireContext())
    }

    private val mSignatureCourseAdapter: SignatureCourseAdapter by lazy {
        SignatureCourseAdapter(requireContext())
    }
    private lateinit var binding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showDialogIntroduce()
        Log.d("TOKEN", getToken())
        showCategories()
        showCollections()
        showInstructors()
        showClassLive()
        showCourse()
        showChallenges()
        showSignatureCourses()

    }

    private fun getToken(): String {
        return sharePreferenceViewModel.getTokenFromPreference(KEY_TOKEN)
    }

    private fun showCategories() {
        homeViewModel.getCategories(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listCategories ->
                            mViewPagerAdapter.setData(listCategories)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })

        binding.root.view_pager.adapter = mViewPagerAdapter
    }

    private fun showCollections() {
        binding.root.recycler_collection.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_collection.layoutManager = layoutManager

        homeViewModel.getCollections(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listCollection ->
                            mCollectionAdapter.setCollection(listCollection)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_collection.adapter = mCollectionAdapter
    }

    private fun showInstructors() {
        binding.root.recycler_instructor.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_instructor.layoutManager = layoutManager

        homeViewModel.getInstructors(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listInstructor ->
                            mInstructorAdapter.setInstructor(listInstructor)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_instructor.adapter = mInstructorAdapter
    }

    private fun showClassLive() {
        binding.root.recycler_class_live.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_class_live.layoutManager = layoutManager

        homeViewModel.getClassLives(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listLiveClass ->
                            mLiveClassAdapter.setClassLive(listLiveClass)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_class_live.adapter = mLiveClassAdapter
    }

    private fun showCourse() {
        binding.root.recycler_course.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_course.layoutManager = layoutManager

        homeViewModel.getCourses(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listCourse ->
                            mCourseAdapter.setCourses(listCourse)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_course.adapter = mCourseAdapter
    }

    private fun showChallenges() {
        binding.root.recycler_challenge.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_challenge.layoutManager = layoutManager

        homeViewModel.getChallenges(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listChallenge ->
                            mChallengeAdapter.setChallenge(listChallenge)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_challenge.adapter = mChallengeAdapter
    }

    private fun showSignatureCourses() {
        binding.root.recycler_signature_course.setHasFixedSize(true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.root.recycler_signature_course.layoutManager = layoutManager

        homeViewModel.getSignatureCourse(getToken()).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { listSignatureCourse ->
                            mSignatureCourseAdapter.setSignatureCourse(listSignatureCourse)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
        binding.root.recycler_signature_course.adapter = mSignatureCourseAdapter
    }

    private val onClickButtonLive: (String) -> Unit = {
        val uri = Uri.parse(it)
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    }

    private fun showDialogIntroduce() {
        DialogSuggestionFragment.newInstance()
            .show(requireActivity().supportFragmentManager, "suggestion")
    }

    companion object {
        const val KEY_TOKEN = "token"
    }
}