package com.minhchaudm.majesticwoman.ui.model

data class User (
    val id: Int,
    val first_name: String,
    val last_name: String,
    val email: String,
    val stripe_customer_id: String
        )