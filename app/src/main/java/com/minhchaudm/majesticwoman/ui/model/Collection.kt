package com.minhchaudm.majesticwoman.ui.model

data class Collection(
    val id: Int,
    val title: String,
    val description: String,
    val is_favorite: Boolean,
    val image: ObjectImage
)