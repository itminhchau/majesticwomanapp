package com.minhchaudm.majesticwoman.ui.model

data class SignIn (val email: String, val password: String)