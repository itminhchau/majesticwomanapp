package com.minhchaudm.majesticwoman.ui.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.activity.MainActivity
import com.minhchaudm.majesticwoman.ui.model.SignIn
import com.minhchaudm.majesticwoman.utils.Status
import com.minhchaudm.majesticwoman.viewmodel.SharePreferenceViewModel
import com.minhchaudm.majesticwoman.viewmodel.SignInViewModel
import kotlinx.android.synthetic.main.fragment_signin.view.*

class SignInFragment : Fragment() {
    private val loginViewModel: SignInViewModel by lazy {
        ViewModelProvider(
            this,
            SignInViewModel.LoginViewModelFactory(requireActivity().application)
        )[SignInViewModel::class.java]
    }
    private val sharePreferenceViewModel: SharePreferenceViewModel by lazy {
        ViewModelProvider(
            this,
            SharePreferenceViewModel.SharePreferenceViewModelFactory(requireActivity().application)
        )[SharePreferenceViewModel::class.java]
    }
    private lateinit var binding: ViewDataBinding
    private val navController by lazy {
        findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.root.button_sign_in.setOnClickListener {
            signIn();

        }

    }

    private fun signIn() {
        val editTextEmail = binding.root.editText_email.text.toString().trim();
        val editTextPassword = binding.root.editText_password.text.toString().trim()
        if (TextUtils.isEmpty(editTextEmail) && TextUtils.isEmpty(editTextPassword)) {
            Toast.makeText(requireContext(), "Please Enter Email and Password", Toast.LENGTH_SHORT)
                .show()
            return
        }
        val login = SignIn(editTextEmail, editTextPassword)
        loginViewModel.signInUser(login).observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let {

                            sharePreferenceViewModel.setTokenToPreference("token", it.token)
                            (requireActivity() as MainActivity).showBottomNav()
                            navController.navigate(R.id.action_signInFragment_to_homeFragment)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), "false", Toast.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })

    }

    companion object {
        fun newInstance() = SignInFragment()
    }
}
