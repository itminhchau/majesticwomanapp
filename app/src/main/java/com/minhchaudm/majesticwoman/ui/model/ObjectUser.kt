package com.minhchaudm.majesticwoman.ui.model

data class ObjectUser (
    val user: User,
    val token: String
    )