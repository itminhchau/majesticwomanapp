package com.minhchaudm.majesticwoman.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.minhchaudm.majesticwoman.repository.Repository
import com.minhchaudm.majesticwoman.ui.model.Register
import com.minhchaudm.majesticwoman.utils.Resource
import kotlinx.coroutines.Dispatchers

class RegisterViewModel(app: Application) : ViewModel() {
    private val repository: Repository = Repository(app)

    fun registerUser(register: Register) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.registerUser(register)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    class RegisterViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(RegisterViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return RegisterViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}