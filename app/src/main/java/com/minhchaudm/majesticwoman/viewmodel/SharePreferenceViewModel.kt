package com.minhchaudm.majesticwoman.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.minhchaudm.majesticwoman.repository.Repository

class SharePreferenceViewModel(app: Application) : ViewModel() {
    private val repository: Repository = Repository(app)


    fun setTokenToPreference(key: String, value: String) =
        repository.setTokenToPreference(key, value)

    fun getTokenFromPreference(key: String): String {

        return repository.getTokenFromPreference(key)
    }

    fun removeTokenFromPreference(key: String) {
        repository.removeTokenFromPreference(key)
        getTokenFromPreference(key)
    }

    class SharePreferenceViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SharePreferenceViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SharePreferenceViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}