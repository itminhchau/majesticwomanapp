package com.minhchaudm.majesticwoman.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.minhchaudm.majesticwoman.repository.Repository
import com.minhchaudm.majesticwoman.utils.Resource
import kotlinx.coroutines.Dispatchers

class HomeViewModel(app: Application) : ViewModel() {
    private val repository: Repository = Repository(app)

    fun getCategories(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getCategories(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getCollections(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getCollections(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getInstructors(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getInstructors(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getClassLives(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getClassLives(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getCourses(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getCourses(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getChallenges(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getChallenges(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    fun getSignatureCourse(token: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getSignatureCourses(token)))
        } catch (e: Exception) {
            emit(Resource.error(null, e.message ?: "error"))
        }
    }

    class HomeViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return HomeViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }

    }
}