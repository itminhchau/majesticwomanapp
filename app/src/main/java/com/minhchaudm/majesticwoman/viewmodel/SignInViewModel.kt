package com.minhchaudm.majesticwoman.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.minhchaudm.majesticwoman.repository.Repository
import com.minhchaudm.majesticwoman.ui.model.SignIn
import com.minhchaudm.majesticwoman.utils.Resource
import kotlinx.coroutines.Dispatchers

class SignInViewModel(app: Application) : ViewModel() {
    private val repository: Repository = Repository(app)

    fun signInUser(signIn: SignIn) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.signInUser(signIn)))
        } catch (e: Exception) {

            emit(Resource.error(null, e.message ?: "Error !!!"))
        }
    }

    class LoginViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SignInViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SignInViewModel(application) as T
            }

            throw IllegalArgumentException("Unable construct viewModel")
        }
    }

}