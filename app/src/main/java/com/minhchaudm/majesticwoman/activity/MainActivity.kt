package com.minhchaudm.majesticwoman.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.isShowBottomNav = View.GONE
        navController = findNavController(R.id.fragment_container_view)
        binding.bottomNav.setOnItemSelectedListener { item ->
            val navigationBuilder = NavOptions.Builder()
            if (item.itemId == R.id.homeFragment) {
                navigationBuilder.setPopUpTo(R.id.homeFragment, true)
            }
            navController.navigate(item.itemId, null, navigationBuilder.build())
            true
        }
        setOnDestination()
    }

    fun showBottomNav() {
        binding.isShowBottomNav = View.VISIBLE
    }

    private fun setOnDestination() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            binding.root.bottom_nav.menu.forEach {
                if (matchDestination(destination, it.itemId)) {
                    it.isChecked = true
                }
            }
        }
    }

    private fun matchDestination(destination: NavDestination, destId: Int): Boolean {
        var currentDestination = destination
        while (currentDestination.id != destId && currentDestination.parent != null) {
            currentDestination = currentDestination.parent!!;
        }
        return currentDestination.id == destId;
    }
}
