package com.minhchaudm.majesticwoman.utils

enum class Status {
    ERROR,
    SUCCESS,
    LOADING
}