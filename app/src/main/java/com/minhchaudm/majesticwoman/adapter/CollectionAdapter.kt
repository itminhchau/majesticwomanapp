package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.Collection

class CollectionAdapter(
    private val context: Context
) : RecyclerView.Adapter<CollectionAdapter.NoteViewHolder>() {
    private var listCollection: List<Collection> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val collectionName: TextView = itemView.findViewById(R.id.item_text_collection)
        private val collectionDescription: TextView =
            itemView.findViewById(R.id.item_text_description_collection)
        private val imageCollection: ImageView = itemView.findViewById(R.id.item_image_collection)
        fun onBind(data: Collection) {
            collectionName.text = data.title
            collectionDescription.text = data.description
            Glide.with(context)
                .load(data.image.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(imageCollection)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_collection, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listCollection.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listCollection[position])
    }

    fun setCollection(listCollection: List<Collection>) {
        this.listCollection = listCollection
        notifyDataSetChanged()
    }

    fun getCollection(position: Int): String {
        return listCollection[position].toString()
    }
}