package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.Category
import kotlinx.android.synthetic.main.item_view_pager.view.*

class ViewPagerAdapter(val context: Context) : PagerAdapter() {
    private var listCategory: List<Category> = listOf()
    fun setData(listPhoto: List<Category>) {
        this.listCategory = listPhoto
        notifyDataSetChanged()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.item_view_pager, container, false)
        val categories = listCategory[position]
        if (categories != null) {
            Glide.with(context)
                .load(categories.image.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(view.item_image)
            view.item_text_categories.text = categories.title
            val course = "${categories.courses_count} Courses"
            view.item_text_course_categories.text = course
        }
        //addview
        container.addView(view)
        return view
    }

    override fun getCount(): Int {
        return listCategory.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}