package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.Challenge

class ChallengeAdapter(
    private val context: Context
) : RecyclerView.Adapter<ChallengeAdapter.NoteViewHolder>() {
    private var listChallenge: List<Challenge> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageChallenge: ImageView = itemView.findViewById(R.id.item_image_challenge)
        fun onBind(data: Challenge) {

            Glide.with(context)
                .load(data.image.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(imageChallenge)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_challenge, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listChallenge.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listChallenge[position])
    }

    fun setChallenge(listChallenge: List<Challenge>) {
        this.listChallenge = listChallenge
        notifyDataSetChanged()
    }

    fun getChallenge(position: Int): String {
        return listChallenge[position].toString()
    }
}