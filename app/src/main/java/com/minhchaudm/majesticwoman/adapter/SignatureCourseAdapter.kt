package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.SignatureCourse

class SignatureCourseAdapter(
    private val context: Context
) : RecyclerView.Adapter<SignatureCourseAdapter.NoteViewHolder>() {
    private var listSignatureCourse: List<SignatureCourse> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val instructorName: TextView =
            itemView.findViewById(R.id.item_text_name_instructor_signature_course)
        private val signatureCourseDescription: TextView =
            itemView.findViewById(R.id.item_text_description_signature_course)
        private val signatureCourseImage: ImageView =
            itemView.findViewById(R.id.item_image_signature_course)
        private val signatureCourseName: TextView =
            itemView.findViewById(R.id.item_text_name_signature_course)
        private val category: TextView =
            itemView.findViewById(R.id.item_text_categories_signature_course)

        fun onBind(data: SignatureCourse) {
            val name = "${data.teacher.first_name} ${data.teacher.last_name}"
            instructorName.text = name

            category.text = data.category.title
            signatureCourseDescription.text = data.description
            signatureCourseName.text = data.title


            Glide.with(context)
                .load(data.image.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(signatureCourseImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_course, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listSignatureCourse.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listSignatureCourse[position])
    }

    fun setSignatureCourse(listSignatureCourse: List<SignatureCourse>) {
        this.listSignatureCourse = listSignatureCourse
        notifyDataSetChanged()
    }

    fun getSignatureCourse(position: Int): String {
        return listSignatureCourse[position].toString()
    }
}