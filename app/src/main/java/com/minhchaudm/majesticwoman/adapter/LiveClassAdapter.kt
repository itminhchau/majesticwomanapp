package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.LiveClass

class LiveClassAdapter(
    private val context: Context,
    private val onClickButtonLive: (String) -> Unit
) : RecyclerView.Adapter<LiveClassAdapter.NoteViewHolder>() {
    private var listLiveClass: List<LiveClass> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val classLiveName: TextView = itemView.findViewById(R.id.item_text_name_live_class)
        private val instructorName: TextView =
            itemView.findViewById(R.id.item_text_name_instructor_live)
        private val imageClassLive: ImageView = itemView.findViewById(R.id.item_image_live_class)
        private val shorDescription: TextView =
            itemView.findViewById(R.id.item_text_description_live)
        private val imageButtonLive: ImageView = itemView.findViewById(R.id.item_live_button)
        fun onBind(data: LiveClass) {
            val name = "${data.teacher.first_name} ${data.teacher.first_name}"
            instructorName.text = name
            classLiveName.text = data.title
            Glide.with(context)
                .load(data.thumbnail.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(imageClassLive)
            shorDescription.text = data.description
            imageButtonLive.setOnClickListener {
                onClickButtonLive(data.live_url)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_live_class, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listLiveClass.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listLiveClass[position])
    }

    fun setClassLive(listLiveClass: List<LiveClass>) {
        this.listLiveClass = listLiveClass
        notifyDataSetChanged()
    }

    fun getClassLive(position: Int): String {
        return listLiveClass[position].toString()
    }
}