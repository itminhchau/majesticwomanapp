package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.Course

class CourseAdapter(
    private val context: Context
) : RecyclerView.Adapter<CourseAdapter.NoteViewHolder>() {
    private var listCourse: List<Course> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val instructorName: TextView =
            itemView.findViewById(R.id.item_text_name_instructor_course)
        private val courseDescription: TextView =
            itemView.findViewById(R.id.item_text_description_course)
        private val courseImage: ImageView = itemView.findViewById(R.id.item_image_course)
        private val lesson: TextView = itemView.findViewById(R.id.item_text_name_lessons_course)
        private val courseName: TextView = itemView.findViewById(R.id.item_text_name_course)
        private val categoryCollection: TextView =
            itemView.findViewById(R.id.item_text_categories_collection_course)

        fun onBind(data: Course) {
            val name = "${data.teacher.first_name} ${data.teacher.last_name}"
            instructorName.text = name

            val numberLesson = "${data.videos.size} lessons"
            lesson.text = numberLesson
            categoryCollection.text = data.category.title
            courseDescription.text = data.description
            courseName.text = data.title


            Glide.with(context)
                .load(data.image.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(courseImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_course, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listCourse.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listCourse[position])
    }

    fun setCourses(listCourse: List<Course>) {
        this.listCourse = listCourse
        notifyDataSetChanged()
    }

    fun getCourses(position: Int): String {
        return listCourse[position].toString()
    }
}