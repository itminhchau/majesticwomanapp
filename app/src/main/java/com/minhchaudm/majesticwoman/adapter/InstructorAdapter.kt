package com.minhchaudm.majesticwoman.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.minhchaudm.majesticwoman.R
import com.minhchaudm.majesticwoman.ui.model.Instructor

class InstructorAdapter(
    private val context: Context
) : RecyclerView.Adapter<InstructorAdapter.NoteViewHolder>() {
    private var listInstructor: List<Instructor> = listOf()

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val instructorName: TextView = itemView.findViewById(R.id.item_text_name_instructor)
        private val collectionDescription: TextView =
            itemView.findViewById(R.id.item_text_description_instruction)
        private val imageCollection: ImageView = itemView.findViewById(R.id.item_image_instructor)
        private val textcourse: TextView = itemView.findViewById(R.id.item_text_course_instructor)
        fun onBind(data: Instructor) {
            val name = "${data.first_name} ${data.last_name}"
            instructorName.text = name
            val course = "${data.courses_count} Courses"
            textcourse.text = course
            Glide.with(context)
                .load(data.thumbnail.file)
                .placeholder(ColorDrawable(Color.BLACK))
                .into(imageCollection)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemNoteView: View =
            LayoutInflater.from(context).inflate(R.layout.item_instructor, parent, false)
        return NoteViewHolder(itemNoteView)
    }

    override fun getItemCount(): Int = listInstructor.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(listInstructor[position])
    }

    fun setInstructor(listInstructor: List<Instructor>) {
        this.listInstructor = listInstructor
        notifyDataSetChanged()
    }

    fun getInstructor(position: Int): String {
        return listInstructor[position].toString()
    }
}