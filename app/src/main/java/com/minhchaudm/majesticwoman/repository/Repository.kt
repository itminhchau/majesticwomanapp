package com.minhchaudm.majesticwoman.repository

import android.app.Application
import com.minhchaudm.majesticwoman.data.api.ApiConfig
import com.minhchaudm.majesticwoman.data.local.DataTokenSharePreference
import com.minhchaudm.majesticwoman.ui.model.SignIn
import com.minhchaudm.majesticwoman.ui.model.Register

class Repository(app: Application) {
    private val application = app
    suspend fun signInUser(signIn: SignIn) = ApiConfig.service.signInUser(signIn)

    suspend fun registerUser(register: Register) = ApiConfig.service.registerUser(register)

    suspend fun getCategories(token: String) = ApiConfig.service.getCategories(token)

    suspend fun getCollections(token: String) = ApiConfig.service.getCollections(token)

    suspend fun getInstructors(token: String) = ApiConfig.service.getInstructor(token)

    suspend fun getClassLives(token: String) = ApiConfig.service.getClassLives(token)

    suspend fun getCourses(token: String) = ApiConfig.service.getCourse(token)

    suspend fun getChallenges(token: String) = ApiConfig.service.getChallenge(token)

    suspend fun getSignatureCourses(token: String) = ApiConfig.service.getSignatureCourse(token)

    fun setTokenToPreference(key: String, value: String) =
        DataTokenSharePreference.newInstance(application).setString(key, value)

    fun getTokenFromPreference(key: String): String =
        DataTokenSharePreference.newInstance(application).getString(key)

    fun removeTokenFromPreference(key: String) =
        DataTokenSharePreference.newInstance(application).removeString(key)
}